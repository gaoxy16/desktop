﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeskTopServer
{
    public class ClientCmd
    {
        public string action { get; set; }
        public int x { get; set; }
        public int y { get; set; }
        public int key { get; set; }
        public bool shift { get; set; }
        public bool ctrl { get; set; }
        public bool alt { get; set; }
    }
   

}
